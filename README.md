# tgaussian

My personal gaussian scripts for doing calculations.

To use import module to pythonpath, assuming installed to home directory

```bash
export PYTHONPATH=$PYTHONPATH:$HOME/tgaussian/
export PATH=$HOME/tgaussian/
```

Example of how to get if an imaginary mode exists

```python
from tgaussian import GaussLog
calculation = GaussLog("calc.log")
has_imaginary_mode = calculation.get_IMODE()
```
