import numpy as np
import re
from ase import Atoms, Atom
from ase.calculators.singlepoint import SinglePointCalculator


solvents={
        "Water": 78.3553,
        "Acetonitrile": 35.688,
        "Methanol": 32.613,
        "Ethanol": 24.852,
        "Toluene": 2.3741,
        "Heptane": 1.9113,
        "Acetone": 20.493,
        "Formamide": 108.94,
        "Pyridine": 12.978,
        "n-Hexane": 1.8819,
        "Chloroform": 4.7113,
        "Dichloromethane": 8.93,
        "AceticAcid": 6.2528,
        "AcetoPhenone": 17.44,
        "DiMethylSulfoxide": 46.826,
        "1,2-EthaneDiol": 40.245,
        "TetraHydroThiophene-s,s-dioxide": 43.962,
        "n-MethylFormamide-mixture": 181.56,
        "n-Pentane":1.8371,
        "Benzene": 2.2706,
        "Argon": 1.430,
        "1-FluoroOctane": 3.89,
        "DiEthylSulfide": 5.723,
        "DiIodoMethane": 5.32,
        "Propanal": 18.5,
        "Vacuum": 1,
}	


def solvent_filename(name):
    return re.sub("[^A-Za-z0-9._]", "_", name)

class GaussVibrations(object):
    def __init__(self, gausslog):
        self.gausslog = gausslog
        self.freqs = []
        self.force_consts = []
        self.IR_intensities = []
        self.modes = []
 
        # Read vibrations
        lines = self.gausslog.lines
        for index in range(len(lines)):
            if "Frequencies --" in lines[index]:
                count = len(lines[index].split()) - 2
                for c in range(count):
                    freq = None
                    reduced_mass = None
                    force_const = None
                    IR_intensity = None
                    working_index = index
                    while("Atom" not in lines[working_index]):
                        # Reading freq data
                        parts = lines[working_index].split()
                        if parts[0] == "Frequencies":
                            freq = float(parts[2+c])
                        elif parts[0] == "Red.":
                            reduced_mass = float(parts[3+c])
                        elif parts[0] == "Frc":
                            force_const = float(parts[3+c])
                        elif parts[0] == "IR":
                            IR_intensity = float(parts[3+c])
                        else:
                            raise Exception("Unknown problem in freq format")
                        working_index += 1
                    mode = []
                    for atom_index in range(len(self.gausslog.get_atoms())):
                        working_index += 1
                        parts = lines[working_index].split()
                        mode.append([float(parts[2+c*3]), float(parts[3+c*3]), float(parts[4+c*3])])
                    self.freqs.append(np.array(freq))
                    self.force_consts.append(np.array(force_const))
                    self.IR_intensities.append(np.array(IR_intensity))
                    self.modes.append(np.array(mode))        


    def displace_mode(self, index, scale=1):
        atoms = self.gausslog.get_atoms().copy()
        atoms.positions += self.modes[index] * scale
        return atoms

    
    def displace_mode_movie(self, index, scale=1, resolution=0.1):
        movie = []
        for this_scale in np.linspace(-scale, scale, int(scale/resolution)*2 + 1):
            movie.append(self.displace_mode(index, scale=this_scale))
        return movie


class GaussLog(object):
    def __init__(self, filename):
        self.filename = filename
        if self.filename is None:
            self.lines = [""]
        else:
            self.lines = open(filename).readlines()


    def get_BSSE(self):
        """
        Gets BSSE correction, this should always be a positive value.

        Example:
        Counterpoise: BSSE energy =       0.001670301642
        """
        try:
            return self.BSSE
        except AttributeError:
            self.BSSE = None
            for line in self.lines:
                if "Counterpoise: BSSE energy =" in line:
                    self.BSSE = float(line.strip().split()[4])
        
        return self.get_BSSE()


    def get_mulliken_charges(self):
        """
        Gets mulliken charges as array.

        Example:
        Mulliken atomic charges:
                     1
            1  I    -1.000000
        Sum of Mulliken atomic charges =  -1.00000
        """
        try:
            return self.mulliken
        except AttributeError:
            self.mulliken = None

            for index, line in enumerate(self.lines):
                if " Mulliken atomic charges:" in line:
                    self.mulliken = []
                    index += 2
                    while("Sum" not in self.lines[index]):
                        self.mulliken.append(float(self.lines[index].split()[2]))
                        index += 1

        return self.get_mulliken_charges()


    def get_APT_charges(self):
        """
        Gets APT charges as array.

        Example:
        APT atomic charges:
                     1
            1  I    -1.000000
        Sum of APT charges =  -1.00000
        """
        try:
            return self.APT
        except AttributeError:
            self.APT = None

            for index, line in enumerate(self.lines):
                if " APT atomic charges:" in line:
                    self.APT = []
                    index += 2
                    while("Sum" not in self.lines[index]):
                        self.APT.append(float(self.lines[index].split()[2]))
                        index += 1

        return self.get_APT_charges()


    def get_ESP_charges(self):
        """
        Gets APT charges as array.
        """
        try:
            return self.ESP
        except AttributeError:
            self.ESP = None

            for index, line in enumerate(self.lines):
                if " Fitting point charges to electrostatic potential" in line:
                    self.ESP = []
                    index += 4
                    while("Charges from ESP fit " not in self.lines[index]):
                        self.ESP.append(float(self.lines[index].split()[2]))
                        index += 1

        return self.get_ESP_charges()


    def get_NBO_charges(self):
        """
        Gets NBO charges as array.

        Example:
        Summary of Natural Population Analysis:

                                              Natural Population
                       Natural  -----------------------------------------------
           Atom  No    Charge         Core      Valence    Rydberg      Total
        -----------------------------------------------------------------------
             C    1    0.53986      1.99919     3.42965    0.03130     5.46014
        =======================================================================
        """
        try:
            return self.NBO
        except AttributeError:
            self.NBO = None

            for index, line in enumerate(self.lines):
                if " Summary of Natural Population Analysis:" in line:
                    self.NBO = []
                    index += 6
                    while("==================================" not in self.lines[index]):
                        self.NBO.append(float(self.lines[index].split()[2]))
                        index += 1

        return self.get_NBO_charges()


    def _find_thermodynamics(self):
        """
        Internal function to get all thermodynamic properties from freq calculation.
        Example:
        ZPE = Zero-point correction=                           0.064400 (Hartree/Particle)
        G = Sum of electronic and zero-point Energies=          -1269.584102
        I = Frequencies --    37.6732                54.3755                80.6118
        """
        self.ZPE, self.G, self.E, self.IMODE = None, None, None, False

        for line in self.lines:
            if "Zero-point correction=" in line:
                self.ZPE = float(line.split()[2])
            if "Sum of electronic and zero-point Energies=" in line:
                self.G = float(line.split()[6])
            if "Frequencies -- " in line:
                if line.count("-") > 2:
                    self.IMODE = True

        if self.G is not None and self.ZPE is not None:
            self.E = self.G - self.ZPE


    def get_normal_terminations(self):
        """
        Gets number of gaussian tasks terminated normally. 
        A negative number implies the the last task did not terminate.
        """
        try:
            return self.terminations
        except AttributeError:
            self.terminations = 0
            for index, line in enumerate(self.lines):
                if "Normal termination of Gaussian 09" in line:
                    self.terminations -= 1
                    if index + 1 == len(self.lines):
                        self.terminations *= -1
                
        return self.get_normal_terminations()


    def _find_eigenvalues(self):
        """
        Internal function to find eigenvalues of molecular orbitals.
        """
        self.a_occ, self.a_virt, self.b_occ, self.b_virt = [], [], [], []
        for index, line in enumerate(self.lines):
            if "Population analysis using the SCF density." in line:
                self.a_occ, self.a_virt, self.b_occ, self.b_virt = [], [], [], []
                index += 4
                while("eigenvalues" not in self.lines[index]):
                    index += 1
                while("eigenvalues" in self.lines[index]):
                    this_line = self.lines[index]
                    for spacer in list(range(28, len(self.lines), 10))[::-1]:
                        this_line = this_line[:spacer] + " " + this_line[spacer:]
                    parts = this_line.split()
                    for part in parts[5:]:
                        if parts[0] in "Alpha":
                            if parts[1] in "occ.":
                                self.a_occ.append(float(part))
                            elif parts[1] in "virt.":
                                self.a_virt.append(float(part))
                        elif parts[0] in "Beta":
                            if parts[1] in "occ.":
                                self.b_occ.append(float(part))
                            elif parts[1] in "virt.":
                                self.b_virt.append(float(part))
                    index += 1
        self.a_occ = np.array(self.a_occ)
        self.a_virt = np.array(self.a_virt)
        self.b_occ = np.array(self.b_occ)
        self.b_virt = np.array(self.b_virt)

        if len(self.a_occ) or len(self.b_occ):
            self.HOMO = np.concatenate((self.a_occ, self.b_occ)).max()
        else:
            self.HOMO = None

        if len(self.a_virt) or len(self.b_virt):
            self.LUMO = np.concatenate((self.a_virt, self.b_virt)).min()
        else:
            self.LUMO = None       
 

    def get_eigenvalues(self):
        """
        Gets eigenvalues of molecular orbitals.
        """
        try:
            return (self.a_occ, self.a_virt, self.b_occ, self.b_virt)
        except AttributeError:
            self._find_eigenvalues(self)

        return self.get_eigenvalues()
    

    def get_LUMO(self):
        try:
            return self.LUMO
        except AttributeError:
            self._find_eigenvalues()
   
        return self.get_LUMO()


    def get_HOMO(self):
        try:
            return self.HOMO
        except AttributeError:
            self._find_eigenvalues()

        return self.get_HOMO() 


    def get_error_terminations(self):
        """
        Gets number of gaussian tasks terminated abnormally.
        """
        try:
            return self.errors
        except AttributeError:
            self.errors = 0
            for line in self.lines:
                if "Error termination" in line:
                    self.errors += 1

        return self.get_error_terminations()

 
    def get_converged(self):
        """
        Gets if geometry optimization converged.
        """
        try:
            return self.converged
        except AttributeError:
            self.converged = False
            for line in self.lines:
                if "Stationary point found" in line:
                    self.converged = True
        
        return self.get_converged()


    def get_IMODE(self):
        """
        Gets if there are any imaginary modes.
        """
        try:
            return self.IMODE
        except AttributeError:
            self._find_thermodynamics()

        return self.get_IMODE()


    def get_E(self, eV=False):
        """
        Gets enthalpy energy.

        if eV is False, returns in Hartree
        """
        try:
            if self.E is None:
                return None
            return self.E * 27.2114 if eV else self.E
        except AttributeError:
            self._find_thermodynamics()

        return self.get_E(eV=eV)


    def get_G(self, eV=False):
        """
        Gets gibbs free energy.

        if eV is False, returns in Hartree
        """
        try:
            if self.G is None:
                return None
            return self.G * 27.2114 if eV else self.G
        except AttributeError:
            self._find_thermodynamics()

        return self.get_G(eV=eV)


    def get_ZPE(self, eV=False):
        """
        Gets zero point energy.

        if eV is False, returns in Hartree
        """
        try:
            if self.ZPE is None:
                return None
            return self.ZPE * 27.2114 if eV else self.ZPE
        except AttributeError:
            self._find_thermodynamics()

        return self.get_ZPE(eV=eV)


    def _find_TD_excitations(self):
        """
        Internal function to get TD excitations

        Excited State   2:      Singlet-A      2.7934 eV  443.85 nm  f=0.1918  <S**2>=0.000
        """
        self.TD_excitations_types = []
        self.TD_excitations_energies = []
        self.TD_excitations_wavelengths = []
        self.TD_excitations_strengths = []
        self.TD_excitations_spin_contaiminations = []

        for line in self.lines:
            if "Excited State " in line:
                parts = line.strip().split()

                self.TD_excitations_types.append(parts[3])
                self.TD_excitations_energies.append(float(parts[4]))
                self.TD_excitations_wavelengths.append(float(parts[6]))
                self.TD_excitations_strengths.append(float(parts[8].split("=")[1]))
                self.TD_excitations_spin_contaiminations.append(float(parts[9].split("=")[1]))

        if not self.TD_excitations_types:
            self.TD_excitations_types = None
            self.TD_excitations_energies = None
            self.TD_excitations_wavelengths = None
            self.TD_excitations_strengths = None
            self.TD_excitations_spin_contaiminations = None


    def get_TD_excitations(self):
        """
        Returns Time Dependent excitation data.

        (Type, eV, nm, strength, spin_contaimination)
        """
        try:
            return (self.TD_excitations_types,
                    self.TD_excitations_energies,
                    self.TD_excitations_wavelengths, 
                    self.TD_excitations_strengths,
                    self.TD_excitations_spin_contaiminations)
        except AttributeError:
            self._find_TD_excitations()

        return self.get_TD_excitations()


    def get_TD_spectra(self, start=150, end=1000, npts_per_nm=1, width=(1/3099.6), width_scale=1, prefactor=40489.9942104):
        """
        Returns spectra plot of TD data.

        https://gaussian.com/uvvisplot/
        """
        try:
            wavelength = np.array(self.TD_excitations_wavelengths)
            intensity = np.array(self.TD_excitations_strengths)
            npts = int((end - start) * npts_per_nm)
            width = width * width_scale

            spectrum = np.empty(npts)
            energies = np.linspace(start, end, npts)
            for index, energy in enumerate(energies):
                spectrum[index] = (intensity * np.exp(-((((1/energy) - (1/wavelength))/(width))**2))).sum()

            spectrum_scaled = spectrum * prefactor

            return energies, spectrum_scaled
        except AttributeError:
            self._find_TD_excitations()

        return get_TD_spectra(start=start, end=end, npts_per_nm=npts_per_nm, width=width, width_scale=width_scale, prefactor=prefactor)


    def get_vibrations(self):
        return GaussVibrations(self)


    def get_atoms(self):
        """
        Returns final atoms image.
        """
        try:
            return self.movie[-1]
        except AttributeError:
            self._read_trajectory()

        return self.get_atoms()


    def _read_trajectory(self):
        lines = self.lines

        movie = []
        atoms = None
        energy = 0
        forces = []

        i = 0
        while(i < len(lines)):
            if "Optimization completed" in lines[i]:
                break
            if "Input orientation:" in lines[i]:
                if atoms is not None:
                    calc = SinglePointCalculator(energy=energy, forces=forces, atoms=atoms)
                    atoms.set_calculator(calc)
                    movie.append(atoms)
                # READ ATOMS
                i += 5
                atoms = Atoms()
                while("---------------------------------------" not in lines[i]):
                    index, number, type, x, y, z = [float(val) for val in lines[i].split()]
                    atoms.extend(Atom(number, position = [x, y, z]))
                    i += 1
            if "Forces (Hartrees/Bohr)" in lines[i]:
                forces = []
                i += 3
                while("---------------------------------------" not in lines[i]):
                    index, number,  x, y, z = [float(val) for val in lines[i].split()]
                    x = x * 51.42208619083232
                    y = y * 51.42208619083232
                    z = z * 51.42208619083232
                    forces.append((x, y, z))
                    i += 1
            if "SCF Done:" in lines[i]:
                energy = float(lines[i].strip().split()[4])
            i += 1

        self.movie = movie



