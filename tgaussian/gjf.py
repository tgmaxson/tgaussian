from ase.constraints import constrained_indices, FixAtoms
from tgaussian import solvents
from tgaussian.logtotraj import read_gauss_log
from ase import Atoms, Atom
from ase.io import read
import re

class GJF():
    def __init__(self, atoms=None):
        self.atoms = atoms
        if isinstance(self.atoms, str):
            try:
                self.atoms = read(self.atoms)
            except:
                self.atoms = read_gauss_log(self.atoms)[-1]

        self.fileplan = None
        self.txt = None
        self.job_type = "optfreq"

        # Default Link 0
        self.chk = "calc.chk"
        self.mem = 15
        self.nproc = 8

        # Gaussian Route
        self.scf = "qc"
        self.xc = "m062x"
        self.basis = "def2tzvpp"
        self.integral = "ultrafine"
        self.opt = {"maxcycles":None, "maxstep":None, "readopt":False}
        self.solvent = None
        self.scrftype = "PCM"
        self.symmetry = "loose"
        self.pop = {"nbo":False, "savenbo":False, "esp":False}
        self.td = {"nstates":10}
        self.route_extra = ""

        # Titlecard
        self.title = "Generated with tgaussian"

        # Charge / Mult
        self.chg = [0]
        self.mult = [1]

        # Output
        self.wfn = None


    def process_tags(self, d):
        tags = []

        for key in d:
            if d[key]:
                if d[key] == True:
                    tags.append("{}".format(key))
                else:
                    val = d[key]
                    tags.append("{}={}".format(key, val))

        if len(tags) == 0:
            return ""

        tags = ",".join(tags)
        tags = "=({})".format(tags)
        return tags


    def read_tags(self, txt):
        txt = txt.replace("(", "")
        txt = txt.replace(")", "")
        data = dict()
        tags = txt.split(",")
        for tag in tags:
            if "=" in tag:
                key, value = tag.split("=")
                try:
                    data[key.lower()] = int(value)
                except ValueError:
                    try:
                        data[key.lower()] = float(value)
                    except ValueError:
                        data[key.lower()] = value.lower()
            else:
                data[tag.lower()] = True
        return data


    def write(self, filename):
        self.fileplan = ["Link0", "Route", "Title", "Atoms"]
        self.txt = ""

        while(len(self.fileplan) > 0):
            self._write_section(self.fileplan.pop(0))

        if filename is "stdout":
            print(self.txt)
        else:
            f = open(filename, "w").write(self.txt)


    def _write_section(self, section):
        sections = {"Link0":self._write_link0,
                    "Route":self._write_route,
                    "Title":self._write_title,
                    "Atoms":self._write_atoms,
                    "WFN":self._write_wfn,
                    "ReadOpt":self._write_readopt}

        sections[section]()


    def _write_link0(self):
        self.txt += "%chk={}\n".format(self.chk)
        self.txt += "%mem={}GB\n".format(self.mem)
        self.txt += "%nproc={}\n".format(self.nproc)
        self.txt += "\n"


    def _write_route(self):
        # Always required
       
        self.txt += "# {xc}/{basis} ".format(xc=self.xc, basis=self.basis)
        if self.scf is not None:
            self.txt += " scf={scf} ".format(scf=self.scf.lower())
        self.txt += "integral=({}) ".format(self.integral.lower())

        # Symmetry
        if self.symmetry is None:
            self.txt += "nosymm "
        else:
            self.txt += "symm={} ".format(self.symmetry.lower())

        # Solvent
        if self.solvent is not None and self.job_type != "cp":
            self.txt += "scrf=({},solvent={})".format(self.scrftype.lower(), self.solvent.lower())

        if self.route_extra:
            self.txt += " {}".format(self.route_extra)

        self.txt += "\n"

        # Job specific options
        if self.job_type == "nbo":
            self.pop = {"nbo":True, "savenbo":True, "esp":False}
            self.job_type = "pop"
        if self.job_type == "esp":
            self.pop = {"nbo":False, "savenbo":False, "esp":True}
            self.job_type = "pop"

        if self.job_type == "optfreq":
            if len(constrained_indices(self.atoms)) > 0:
                self.opt["readopt"] = True
                self.fileplan.append("ReadOpt")

            self.txt += "# opt{} freq\n".format(self.process_tags(self.opt))
            self.txt += "\n"
        elif self.job_type == "cp":
            self.txt += "# counterpoise={}\n".format(len(set([atom.tag for atom in self.atoms])))
            self.txt += "\n"
        elif self.job_type == "pop":
            self.txt += "# pop{}\n".format(self.process_tags(self.pop))
            self.txt += "\n"
        elif self.job_type == "td":
            self.txt += "# TD{}\n".format(self.process_tags(self.td))
            self.txt += "\n"
        elif self.job_type == "wfn":
            self.fileplan.append("WFN")
            self.txt += "# output=WFN\n"
            self.txt += "\n"
        elif self.job_type == "sp":
            self.txt += "# sp\n"
            self.txt += "\n"
            

    def _write_title(self):
        self.txt += "{}\n".format(self.title)
        self.txt += "\n"


    def _write_atoms(self):
        for c, m in zip(self.chg, self.mult):
            self.txt += "{} {} ".format(int(c), int(m))
        self.txt += "\n"
        
        if self.atoms is not None:
            tags = sorted(list(set([atom.tag for atom in self.atoms])))
            for atom in self.atoms:
                atom_label = "{}(Fragment={})".format(atom.symbol, tags.index(atom.tag) + 1)
                self.txt += " {:15s}      {:13.8f} {:13.8f} {:13.8f}\n".format(atom_label, atom.position[0], atom.position[1], atom.position[2])
        self.txt += "\n"


    def _write_wfn(self):
        if not self.wfn:
            self.wfn = "calc.wfn"
        self.txt += "{}".format(self.wfn)
        self.txt += "\n"


    def _write_readopt(self):
        constrained = constrained_indices(self.atoms)
        self.txt += "notatoms=" + ",".join([str(c + 1) for c in constrained]) + "\n"
        self.txt += "\n"


    def load(self, filename, ignore_atoms=False):
        self.lines = open(filename).readlines()
        self.fileplan = ["Link0", "Route", "Title", "Atoms"]
        self.txt = "".join(self.lines)
        
        if ignore_atoms:
            store_atoms = self.atoms
        self.atoms = None

        while(len(self.fileplan) > 0):
            self._load_section(self.fileplan.pop(0))

        if ignore_atoms:
           self.atoms = store_atoms

    def _load_section(self, section):
        sections = {"Link0":self._load_link0,
                    "Route":self._load_route,
                    "Title":self._load_title,
                    "Atoms":self._load_atoms,
                    "WFN":self._load_wfn,
                    "ReadOpt":self._load_readopt}
        sections[section]()


    def _load_link0(self):
        while(self.lines[0] is not "\n"):
            line = self.lines.pop(0)
            if line[0] is not "%":
                raise Exception("Error parsing link0 section")
            key, value = line[1:].strip().split("=")

            if key in "chk":
                self.chk = value
            elif key in "nproc":
                self.nproc = int(value)
            elif key in "mem":
                self.mem = value
                if self.mem[-2:] == "GB":
                    self.mem = int(self.mem[:-2])
        self.lines.pop(0)

    def _load_route(self):
        while(self.lines[0] is not "\n"):
            line = self.lines.pop(0)
            for solvent in solvents:
                line = re.sub(solvent, solvent.replace(",", "_"), line, flags=re.I)
            line = ' '.join(line.split())
            line = line.replace(")", ") ")
            line = line.replace(", ", ",")
            options = line.split()

            for opt in options:
                opt = opt.lower()
                if opt[0] in "#":
                    continue
                elif opt[0:3] in "opt":
                    self.job_type = "optfreq"
                    if "=" in opt:
                        parms = self.read_tags(opt[opt.index("=")+1:])
                        for key in parms:
                            self.opt[key] = parms[key]
                    if self.opt["readopt"]:
                        self.fileplan.append("ReadOpt")
                elif opt[0:4] in "freq":
                    self.job_type = "optfreq"
                elif opt[0:4] in "scrf":
                    if "=" in opt:
                        parms = self.read_tags(opt[opt.index("=")+1:])
                        self.solvent = parms["solvent"]
                elif opt[0:3] in "int":
                    if "=" in opt:
                        parms = self.read_tags(opt[opt.index("=")+1:])
                        for key in parms:
                            if parms[key] == True:
                                self.integral = key
                            else:
                                self.integral = parms[key]
                elif opt[0:3] in "scf":
                    self.scf = opt[opt.index("=")+1:]
                elif opt[0:3] in "sym":
                    self.symmetry = opt[opt.index("=")+1:]
                elif opt[0:5] in "nosym":
                    self.symmetry = None
                elif opt[0:2] in "sp":
                    self.job_type = "sp"
                elif opt[0:3] in "pop":
                    self.job_type = "pop"
                    if "=" in opt:
                        parms = self.read_tags(opt[opt.index("=")+1:])
                        for key in parms:
                            self.pop[key] = parms[key]
                elif opt[0:2] in "td":
                    self.job_type = "td"
                    if "=" in opt:
                        parms = self.read_tags(opt[opt.index("=")+1:])
                        for key in parms:
                            self.td[key] = parms[key]
                elif opt[0:3] in "out":
                    self.job_type = "wfn"
                    self.fileplan.append("WFN")
                elif opt[0:7] in "counter":
                    self.job_type = "cp"
                elif "/" in opt:
                    self.xc, self.basis = opt.split("/")
                else:
                    self.route_extra += opt + ""

        if self.solvent:
            self.solvent = self.solvent.replace("_", ",")

        self.lines.pop(0)


    def _load_title(self):
        self.title = self.lines.pop(0).strip()
        self.lines.pop(0)


    def _load_atoms(self):
        self.atoms = Atoms()
        chgmult = self.lines.pop(0).split()
        self.chg = []
        self.mult = []
        for index in range(0, len(chgmult), 2):
            self.chg.append(int(chgmult[index]))
            self.mult.append(int(chgmult[index + 1]))
        while(self.lines[0] is not "\n"):
            line = self.lines.pop(0)
            element, X, Y, Z = line.split()
            fragment = 0
            if "=" in element:
                element, fragment = element.split("(")
                fragment = fragment.replace("(", "")
                fragment = fragment.replace(")", "")
                fragment = int(fragment.split("=")[-1])
            X, Y, Z = float(X), float(Y), float(Z)
            self.atoms.extend(Atom(element, position=(X, Y, Z), tag=fragment))

        self.lines.pop(0)


    def _load_wfn(self):
        self.wfn = self.lines.pop(0).strip()

        try:
            self.lines.pop(0)
        except IndexError:
            pass

    def _load_readopt(self):
        indices = self.lines.pop(0).split("=")[1].split(",")
        indices = [int(index)-1 for index in indices]
        self.atoms.set_constraint(FixAtoms(indices))

        try:
            self.lines.pop(0)
        except IndexError:
            pass
