#!/usr/bin/env python3
from ase import Atoms, Atom
from ase.calculators.singlepoint import SinglePointCalculator

def read_gauss_log(filename, pes=False):
    lines = open(filename).readlines()

    movie = []
    temp_movie = []
    atoms = None
    energy = 0
    forces = [] 

    i = 0
    while(i < len(lines)):
        if "Optimization completed" in lines[i]:
            if not pes:
                break
            else:
                movie.append(temp_movie[-1])                
        if "Input orientation:" in lines[i]:
            if atoms is not None:
                calc = SinglePointCalculator(energy=energy, forces=forces, atoms=atoms)
                atoms.set_calculator(calc)
                temp_movie.append(atoms)
            # READ ATOMS
            i += 5
            atoms = Atoms()
            while("---------------------------------------" not in lines[i]):
                index, number, type, x, y, z = [float(val) for val in lines[i].split()]
                atoms.extend(Atom(number, position = [x, y, z]))
                i += 1
        if "Forces (Hartrees/Bohr)" in lines[i]:
            forces = []
            i += 3
            while("---------------------------------------" not in lines[i]):
                index, number,  x, y, z = [float(val) for val in lines[i].split()]
                x = x * 51.42208619083232
                y = y * 51.42208619083232
                z = z * 51.42208619083232
                forces.append((x, y, z))
                i += 1
        if "SCF Done:" in lines[i]:
            energy = float(lines[i].strip().split()[4])
        i += 1

    if not pes:
        movie = temp_movie

    return movie

if __name__ == "__main__":
    from ase.io import read, write
    from ase.visualize import view
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("--output", "-o", type=str, default=None)
    parser.add_argument("--last", "-l", action="store_true", default=False)
    parser.add_argument("--tag", "-t", type=str, default=None)
    parser.add_argument("--pes", "-p", action="store_true", default=False)
    parser.add_argument("log", type=str)
    args = parser.parse_args() 
   
    movie = read_gauss_log(args.log, pes=args.pes)

    if args.tag is not None:
        tagging = args.tag
        tags = read(tagging).get_tags()
        print(tags)
        for atoms in movie:
            atoms.set_tags(tags)

    if args.output is not None:
        if args.last:
            write(args.output, movie[-1])
        else:
            write(args.output, movie)
    else:
        view(movie)
